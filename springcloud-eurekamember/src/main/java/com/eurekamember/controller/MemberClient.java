package com.eurekamember.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/11/24 10:02
 * @version: 1.0
 */

@RestController
public class MemberClient {

    @RequestMapping("/memberServer")
    public String memberServer(){
        System.out.println("memberServer");
        return "memberServer";
    }
}
