package com.eurekaorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Author: CPX
 * @Date: 2020/11/24 10:13
 * @version: 1.0
 */
@RestController
public class OrderClient {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/orderServer")
    public String orderServer(){
        System.out.println("orderServer");
        String memberName = "http://app-eureka-member/memberServer";
        String memberResult = restTemplate.getForObject(memberName, String.class);
        return memberResult;
    }
}
