package com.eurekaorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @EnableEurekaClient :声明该服务是eureka客户端，
 * 如果使用rest方式以别名方式进行调用依赖ribbon负载均衡器 @LoadBalanced
 * @LoadBalanced 就能让这个RestTemplate在请求时拥有客户端负载均衡的能力，不加的会提示不识别服务名
 */
@EnableEurekaClient
@SpringBootApplication
public class EurekaorderApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaorderApplication.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate initRestTemplate(){
		return new RestTemplate();
	}

}
