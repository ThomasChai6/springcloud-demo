package com.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 微服务：是将单一的开发工程分为一组小服务进行开发（根据不同模块进行多子项目开发）
 * 		微服务架构倡导应用程序设计程多个独立、可配置、可运行和可微服务的子服务
 * 微服务架构：注册中心、配置管理、断路器、智能路由、微代理、控制总线、全局锁、分布式会话等
 * 			SpringCloud包含众多的子项目
 * 			SpringCloud config 分布式配置中心
 * 			SpringCloud netflix 核心组件
 * 			Eureka:服务治理  注册中心
 * 			Hystrix:服务保护框架
 * 			Ribbon:客户端负载均衡器
 * 			Feign：基于ribbon和hystrix的声明式服务调用组件
 * 			Zuul: 网关组件,提供智能路由、访问过滤等功能。
 * 微服务核心：注册中心
 *
 * @EnableEurekaServer :说明本服务是eureka注册中心（即服务端），其他服务要将服务注册到服务中心来
 * @SpringBootApplication :springboot启动类
 */
@EnableEurekaServer
@SpringBootApplication
public class SpringcloudEurekaserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudEurekaserverApplication.class, args);
	}

}
